package org.volme.common.json.task;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.volme.R;
import org.volme.activity.main.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class JsonProviderTest {

    @Rule
    public IntentsTestRule<MainActivity> mainActivityIntentsTestRule =
            new IntentsTestRule<>(MainActivity.class);

    @Test
    public void parseJsonAndDisplayResultOnMainScreenTest(){

        // given
        int parseButtonId = R.id.main_screen_parse_button;

        // when
        onView(withId(parseButtonId)).perform(click());

        // then
        onView(withId(R.id.main_screen_event_type_editText)).check(matches(not(withText(""))));
        onView(withId(R.id.main_screen_device_number_editText)).check(matches(not(withText(""))));
        onView(withId(R.id.main_screen_location_editText)).check(matches(not(withText(""))));
        onView(withId(R.id.main_screen_date_editText)).check(matches(not(withText(""))));
    }


}