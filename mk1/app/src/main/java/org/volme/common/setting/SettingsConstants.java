package org.volme.common.setting;

public interface SettingsConstants {

    //// SETTINGS RELATED CONSTANTS
    String PREF_NAME = "prefs";
    String KEY_USER_EMAIL_OR_PHONE = "userEmailOrPhone";
    String KEY_PASS = "password";

}
