package org.volme.common.json.task;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import org.jsoup.Jsoup;
import org.volme.activity.main.MainActivity;
import org.volme.common.json.CommonJsonParser;
import org.volme.model.entity.Event;

import java.io.IOException;

import static org.volme.common.AppConstants.EMPTY_STRING;

/**
 * an async task class that communicates with a server by url and gets data from it in JSON format
 **/
public class JsonProvider extends AsyncTask<Context, Void, String> {

    private static final String TAG = "JsonProvider";

    @SuppressLint("StaticFieldLeak")
    private Context context;

    private static final String SERVER_URL = "https://private-b70535-carwave.apiary-mock.com/alert";

    /**
     * method-job of parse task that will be executed in background mode
     * @param contexts - set of transferred context objects where result of job will be placed
     * @return String - json as a string object
     **/
    @Override
    protected String doInBackground(Context... contexts) {
        try {
            this.context = contexts[0];
            return Jsoup.connect(SERVER_URL)
                    .ignoreContentType(true).execute().body();
        } catch (IOException e) {
            Log.e(TAG, "Json was not received from url: " + SERVER_URL);
            return EMPTY_STRING;
        }
    }

    /**
     * a post method that display result of parsing on main screen
     * @param jsonToParse - set of transferred context objects where result of job will be placed
     **/
    @Override
    protected void onPostExecute(String jsonToParse) {
        super.onPostExecute(jsonToParse);
        CommonJsonParser commonJsonParser = new CommonJsonParser();
        Event event = commonJsonParser.parse(jsonToParse);

        MainActivity mainActivity = (MainActivity) context;

        EditText eventTypeEditText = mainActivity.getEventTypeEditText();
        eventTypeEditText.setText(event.getEventType());

        EditText deviceNumberEditText = mainActivity.getDeviceNumberEditText();
        deviceNumberEditText.setText(event.getDeviceNumber());

        EditText locationEditText = mainActivity.getLocationEditText();
        locationEditText.setText(event.getLocation());

        EditText dateEditText = mainActivity.getDateEditText();
        dateEditText.setText(event.getDate());

        mainActivity.hideProgressDialog();
    }

}
