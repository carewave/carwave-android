package org.volme.common;

/**
 * a common interface that contains global app related constants
 **/
public interface AppConstants {

    String EMPTY_STRING = "";
    String RECEIVED_SMS_CODE = "receivedSmsCode";

}
