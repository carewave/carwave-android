package org.volme.common.json;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.volme.model.entity.Event;

import java.util.Optional;

import static org.volme.common.AppConstants.EMPTY_STRING;
import static org.volme.model.entity.JsonEventKeyStorage.DATE;
import static org.volme.model.entity.JsonEventKeyStorage.DEVICE_NUMBER;
import static org.volme.model.entity.JsonEventKeyStorage.EVENT_TYPE;
import static org.volme.model.entity.JsonEventKeyStorage.LOCATION;
import static org.volme.model.entity.factory.EventFactory.createDummyEvent;
import static org.volme.model.entity.factory.EventFactory.createEvent;

/**
 * a common json parse class that uses JSONObject api to parse receiving json content
 **/
public class CommonJsonParser implements JsonParsable {

    private static final String TAG = "CommonJsonParser";

    /**
     * a method-parser that tries to convert json data into application's event object
     * @param incomeJsonContent - json content from where we have to get data
     * @return Event - object that contains parsed json data
     **/
    @Override
    public Event parse(String incomeJsonContent) {
        String jsonToParse = Optional.ofNullable(incomeJsonContent).orElse(EMPTY_STRING);

        if (jsonToParse.isEmpty()){
            return createDummyEvent();
        }

        JSONObject jsonObj;
        Event event;
        try {
            jsonObj = new JSONObject(jsonToParse);
            String eventType     = jsonObj.getString(EVENT_TYPE.toString());
            String deviceNumber  = jsonObj.getString(DEVICE_NUMBER.toString());
            String location      = jsonObj.getString(LOCATION.toString());
            String date          = jsonObj.getString(DATE.toString());
            event = createEvent(eventType, deviceNumber, location, date);
        } catch (JSONException e) {
            Log.e(TAG, "Corrupted JSON received. Returning dummy event object...");
            return createDummyEvent();
        }

        return event;
    }

}
