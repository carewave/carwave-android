package org.volme.common.json;

import org.volme.model.entity.Event;

/**
 * a common interface that sets parse behaviour
 **/
public interface JsonParsable {

    Event parse(String jsonContent);

}
