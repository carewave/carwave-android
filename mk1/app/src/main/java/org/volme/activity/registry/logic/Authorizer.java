package org.volme.activity.registry.logic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

import org.volme.activity.registry.SmsVerificationActivity;
import org.volme.common.db.FireBaseDBInitializer;

import static org.volme.common.setting.SettingsConstants.KEY_USER_EMAIL_OR_PHONE;
import static org.volme.common.setting.SettingsConstants.PREF_NAME;

abstract class Authorizer {

    // Firebase related fields
    FirebaseAuth mAuth;
    FirebaseFirestore firestoreDB;
    private SharedPreferences sharedPreferences;

    public abstract void registerUser();

    /**
     * Starts main activity of the application.
     **/
    void finishSingUpActivity(Activity currentActivity, String emailPhone) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(emailPhone).build();
            user.updateProfile(profileUpdates);
        }

        // db init
        FireBaseDBInitializer.create().init();

        sharedPreferences = currentActivity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        managePrefs(emailPhone);
    }

    private void managePrefs(String emailPhone) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_EMAIL_OR_PHONE, emailPhone.trim());
        editor.apply();
        editor.commit();
    }

}
