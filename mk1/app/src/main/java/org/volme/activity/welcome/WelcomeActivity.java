package org.volme.activity.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import org.volme.R;
import org.volme.activity.registry.RegistryGetStartedActivity;

public class WelcomeActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        progressBar = findViewById(R.id.welcome_progressBar_cyclic);
        hideProgressDialog();
        startRegisterScreen();
    }

    private void startRegisterScreen() {
        showProgressDialog();
        Intent intent = new Intent(this, RegistryGetStartedActivity.class);
        startActivity(intent);
        hideProgressDialog();
    }

    /**
     * Shows progress dialog while backend action is in progress.
     **/
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hides progress dialog from screen.
     **/
    public void hideProgressDialog() {
        progressBar.setVisibility(View.INVISIBLE);
    }


}
