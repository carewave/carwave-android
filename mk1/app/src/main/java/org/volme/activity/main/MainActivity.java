package org.volme.activity.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.volme.R;
import org.volme.common.json.task.JsonProvider;

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private EditText eventTypeEditText;
    private EditText deviceNumberEditText;
    private EditText locationEditText;
    private EditText dateEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.login_progressBar_cyclic);
        progressBar.setVisibility(View.INVISIBLE);

        eventTypeEditText       = findViewById(R.id.main_screen_event_type_editText);
        deviceNumberEditText    = findViewById(R.id.main_screen_device_number_editText);
        locationEditText        = findViewById(R.id.main_screen_location_editText);
        dateEditText            = findViewById(R.id.main_screen_date_editText);
    }

    /**
     * method-handler for parse button on main screen
     * @param view - current view on screen
     **/
    public void onParseButtonClick(View view){
        showProgressDialog();
        new JsonProvider().execute(this);
    }

    /**
     * Shows progress dialog while backend action is in progress.
     **/
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hides progress dialog from screen.
     **/
    public void hideProgressDialog() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    public EditText getEventTypeEditText() {
        return eventTypeEditText;
    }

    public EditText getDeviceNumberEditText() {
        return deviceNumberEditText;
    }

    public EditText getLocationEditText() {
        return locationEditText;
    }

    public EditText getDateEditText() {
        return dateEditText;
    }
}