package org.volme.activity.registry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.volme.R;

public class RegistryGetStartedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry_get_started);
    }

    public void getStartedActivity(View view) {
        Intent intent = new Intent(this, PhoneNumberSetupActivity.class);
        startActivity(intent);
    }


}
