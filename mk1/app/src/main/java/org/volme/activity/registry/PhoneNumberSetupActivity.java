package org.volme.activity.registry;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.volme.R;
import org.volme.activity.registry.logic.PhoneAuthorizer;

import java.util.Objects;

import static org.volme.common.AppConstants.RECEIVED_SMS_CODE;

public class PhoneNumberSetupActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private EditText phoneNumberEditText;

    private static final String SMS_RECEIVED_EVENT = "android.provider.Telephony.SMS_RECEIVED";
    private static final String BUNDLE_PDUS_KEY = "pdus";
    private static final String SMS_VERIFY_FLAG = "is your verification code";
    private static final String SMS_DATA_SPLITTER = "\\D+";

    private Activity currentActivity;

    private BroadcastReceiver mSmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getAction(), SMS_RECEIVED_EVENT)) {
                Bundle bundle = intent.getExtras();
                SmsMessage[] messages;
                if (bundle != null) try {
                    //---retrieve the SMS message received---
                    Object[] pdus = (Object[]) bundle.get(BUNDLE_PDUS_KEY);
                    if (pdus != null) {
                        messages = new SmsMessage[pdus.length];
                        for (int i = 0; i < messages.length; i++) {
                            messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                            String msgBody = messages[i].getMessageBody();
                            if (msgBody != null && msgBody.contains(SMS_VERIFY_FLAG)) {
                                String codeMessage = msgBody.split(SMS_DATA_SPLITTER)[0];
                                PhoneAuthorizer.getInstance().
                                        getVerificationDataFromFirestoreAndVerify(codeMessage);

                                Intent newIntent = new Intent(currentActivity, SmsVerificationActivity.class);
                                newIntent.putExtra(RECEIVED_SMS_CODE, codeMessage);
                                currentActivity.startActivity(newIntent);
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_setup);
        currentActivity = this;

        phoneNumberEditText = findViewById(R.id.phone_number_registry_enter_phone_edit_text);

        progressBar = findViewById(R.id.phone_number_registry_progressBar_cyclic);
        progressBar.setVisibility(View.INVISIBLE);

        updateUi();
    }

    private void updateUi() {
        editToolbar();
        setCursor();
    }

    private void setCursor() {
        phoneNumberEditText.setSelection(phoneNumberEditText.getText().length());
    }

    private void editToolbar() {
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
    }

    public void onNext(View view) {
        showProgressDialog();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(mSmsReceiver, filter);

        String phone = phoneNumberEditText.getText().toString();
        PhoneAuthorizer.init(this, phone).registerUser();
    }

    /**
     * Shows progress dialog while backend action is in progress.
     **/
    public void showProgressDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }

}
