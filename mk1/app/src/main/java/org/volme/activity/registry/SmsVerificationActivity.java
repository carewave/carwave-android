package org.volme.activity.registry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.volme.R;

import static org.volme.common.AppConstants.EMPTY_STRING;
import static org.volme.common.AppConstants.RECEIVED_SMS_CODE;
import static org.volme.common.setting.SettingsConstants.KEY_USER_EMAIL_OR_PHONE;
import static org.volme.common.setting.SettingsConstants.PREF_NAME;

public class SmsVerificationActivity extends AppCompatActivity {

    private TextView smsVerificationDescriptionTextView;
    private EditText firstSmsDigit;
    private EditText secondSmsDigit;
    private EditText thirdSmsDigit;
    private EditText fourthSmsDigit;
    private EditText fifthSmsDigit;
    private EditText sixthSmsDigit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_verification);
        Intent intent = getIntent();
        String receivedSmsCode = intent.getStringExtra(RECEIVED_SMS_CODE);

        smsVerificationDescriptionTextView = findViewById(R.id.activity_sms_verification_description);

        firstSmsDigit = findViewById(R.id.first_sms_digit);
        secondSmsDigit = findViewById(R.id.second_sms_digit);
        thirdSmsDigit = findViewById(R.id.third_sms_digit);
        fourthSmsDigit = findViewById(R.id.fourth_sms_digit);
        fifthSmsDigit = findViewById(R.id.fifth_sms_digit);
        sixthSmsDigit = findViewById(R.id.sixth_sms_digit);

        updateUI(receivedSmsCode);
    }

    private void updateUI(String receivedSmsCode) {
        updateDescriptionWithPhoneNumber();
        updateSmsCode(receivedSmsCode);
    }

    private void updateDescriptionWithPhoneNumber(){
        SharedPreferences sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String phoneNumber = sharedPreferences.getString(KEY_USER_EMAIL_OR_PHONE, EMPTY_STRING);

        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            String description = getResources().getString(R.string.sms_verification_activity_description);
            smsVerificationDescriptionTextView.setText(String.format("%s%s", description, phoneNumber));
        }
    }

    private void updateSmsCode(String receivedSmsCode) {
        if(receivedSmsCode != null && !receivedSmsCode.isEmpty()){
            firstSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(0)));
            secondSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(1)));
            thirdSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(2)));
            fourthSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(3)));
            fifthSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(4)));
            sixthSmsDigit.setText(String.valueOf(receivedSmsCode.charAt(5)));
        }
    }

    public void onResend(View view){

    }

    public void onNext(View view) {
        Intent intent = new Intent(this, CarVerificationActivity.class);
        startActivity(intent);
    }

}
