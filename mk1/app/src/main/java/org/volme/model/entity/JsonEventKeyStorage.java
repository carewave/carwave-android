package org.volme.model.entity;

import android.support.annotation.NonNull;

/**
 * represents fields of json that relate to current device status
 **/
public enum JsonEventKeyStorage {

    EVENT_TYPE {
        @NonNull
        public String toString() {
            return "eventType";
        }
    },

    DEVICE_NUMBER {
        @NonNull
        public String toString() {
            return "deviceNumber";
        }
    },

    LOCATION {
        @NonNull
        public String toString() { return "location"; }
    },

    DATE {
        @NonNull
        public String toString() {
            return "date";
        }
    },

    UNKNOWN {
        @NonNull
        public String toString() {
            return "unknown";
        }
    }
}
