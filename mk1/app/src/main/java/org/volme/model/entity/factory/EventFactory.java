package org.volme.model.entity.factory;

import org.volme.model.entity.Event;

import static org.volme.common.AppConstants.EMPTY_STRING;

/**
 * a common event factory class
 **/
public class EventFactory {

    private static final String DUMMY_EVENT_TYPE    = EMPTY_STRING;
    private static final String DUMMY_DEVICE_NUMBER = EMPTY_STRING;
    private static final String DUMMY_LOCATION      = EMPTY_STRING;
    private static final String DUMMY_DATE          = EMPTY_STRING;

    /**
     * a fabric method that creates common event object
     * @param eventType - a type of event that was received from server concerning certain device
     * @param deviceNumber - an unique device identifier
     * @param location - current location of device
     * @param date - current date
     * @return Event - object that contains parsed json data
     **/
    public static Event createEvent(String eventType, String deviceNumber, String location, String date){
        return new Event(eventType, deviceNumber, location, date);
    }

    /**
     * a fabric method that creates dummy event object with empty values in fields
     * @return Event - object that contains parsed json data
     **/
    public static Event createDummyEvent(){
        return new Event(DUMMY_EVENT_TYPE, DUMMY_DEVICE_NUMBER, DUMMY_LOCATION, DUMMY_DATE);
    }

}