package org.volme.model.entity;

/**
 * a class-wrapper of json content related to current device status
 **/
public class Event {

    private String eventType;
    private String deviceNumber;
    private String location;
    private String date;

    private Event() { }

    public Event(String eventType, String deviceNumber, String location, String date) {
        this.eventType = eventType;
        this.deviceNumber = deviceNumber;
        this.location = location;
        this.date = date;
    }

    public String getEventType() {
        return eventType;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }
}
