package org.volme.common.json;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.volme.model.entity.Event;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.volme.model.entity.factory.EventFactory.createDummyEvent;

@RunWith(JUnit4.class)
public class CommonJsonParserTest {

    private JsonParsable commonJsonParser;

    private String stableJsonContent = "{\"eventType\":\"volume_change\",\"deviceNumber\":\"1\",\"location\":\"ololo\",\"date\":\"1546279870829\"}";

    @Before
    public void setup(){
        commonJsonParser = new CommonJsonParser();
    }

    @Test
    public void setNullJsonContentAndReturnNullTest(){
        // given
        Event dummyEvent = createDummyEvent();

        // when
        Event event = commonJsonParser.parse(null);

        // then
        assertNotNull(event);
        assertEquals(dummyEvent.getEventType(), event.getEventType());
        assertEquals(dummyEvent.getDeviceNumber(), event.getDeviceNumber());
        assertEquals(dummyEvent.getLocation(), event.getLocation());
        assertEquals(dummyEvent.getDate(), event.getDate());
    }

    @Test
    public void setCorruptedJsonContentAndReturnNullTest(){
        // given
        String jsonContentCorrupted = "#$52efetg4%$645tfer";
        Event dummyEvent = createDummyEvent();

        // when
        Event event = commonJsonParser.parse(jsonContentCorrupted);

        // then
        assertNotNull(event);
        assertEquals(dummyEvent.getEventType(), event.getEventType());
        assertEquals(dummyEvent.getDeviceNumber(), event.getDeviceNumber());
        assertEquals(dummyEvent.getLocation(), event.getLocation());
        assertEquals(dummyEvent.getDate(), event.getDate());
    }

    @Test
    public void setStableJsonAndReturnEventObjectTest(){
        // given
        String eventType = "volume_change";
        String deviceNumber = "1";
        String location = "ololo";
        String date = "1546279870829";
        Event event;

        // when
        event = commonJsonParser.parse(stableJsonContent);

        // then
        assertNotNull(event);
        assertEquals(event.getEventType(), eventType);
        assertEquals(event.getDeviceNumber(), deviceNumber);
        assertEquals(event.getLocation(), location);
        assertEquals(event.getDate(), date);
    }
}
